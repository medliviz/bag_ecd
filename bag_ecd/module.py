from bag.design import Module
from bag.math import float_to_si_string

class MOMCapModuleBase(Module):
    """The base design class for a metal-on-metal capacitor.

    Parameters
    ----------
    database : ModuleDB
        the design database object.
    yaml_file : str
        the netlist information file name.
    **kwargs :
        additional arguments
    """

    def __init__(self, database, yaml_file, **kwargs):
        Module.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            w='cap finger width, in meters.',
            l='cap finger length, in meters.',
            nf='number of cap fingers (int).',
            sp='spacing between fingers, in meters.',
            stm='metal start layer',
            spm='metal stop layer',
            )

    def design(self, w=1e-6, l=1e-6, nf=10, sp=1e-6, stm=1, spm=6, sub_type='ptap'):
        pass

    def get_schematic_parameters(self):
        # type: () -> Dict[str, str]
        w = self.params['w']
        l = self.params['l']
        nf = self.params['nf']
        sp = self.params['sp']
        stm = self.params['stm']
        spm = self.params['spm']
        wstr = w if isinstance(w, str) else float_to_si_string(w)
        lstr = l if isinstance(l, str) else float_to_si_string(l)
        nfstr = nf if isinstance(nf, str) else "%s" % int(nf)
        spstr = sp if isinstance(sp, str) else float_to_si_string(sp)
        stmstr = stm if isinstance(stm, str) else "%s" % int(stm)
        spmstr = spm if isinstance(spm, str) else "%s" % int(spm)

        return dict(w=wstr, l=lstr, nf=nfstr, sp=spstr, stm=stmstr, spm=spmstr)

    def get_cell_name_from_parameters(self):
        # type: () -> str
        return 'cap_mom'

    def is_primitive(self):
        # type: () -> bool
        return True

    def should_delete_instance(self):
        # type: () -> bool
        return self.params['w'] == 0 or self.params['l'] == 0 or self.params['nf'] == 0

class AccurateMOMCapModuleBase(Module):
    """The base design class for a accurate metal-on-metal capacitor.

    Parameters
    ----------
    database : ModuleDB
        the design database object.
    yaml_file : str
        the netlist information file name.
    **kwargs :
        additional arguments
    """

    def __init__(self, database, yaml_file, **kwargs):
        Module.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            w='cap finger width, in meters.',
            l='cap finger length, in meters.',
            nf='number of cap fingers (int).',
            sp='spacing between fingers, in meters.',
            stm='metal start layer',
            spm='metal stop layer',
            sub_type='Capacitor substrate type',
            )

    def design(self, w=1e-6, l=1e-6, nf=10, sp=1e-6, stm=1, spm=6, sub_type='ptap'):
        pass

    def get_schematic_parameters(self):
        # type: () -> Dict[str, str]
        w = self.params['w']
        l = self.params['l']
        nf = self.params['nf']
        sp = self.params['sp']
        stm = self.params['stm']
        spm = self.params['spm']
        sub_type = self.params['sub_type']
        wstr = w if isinstance(w, str) else float_to_si_string(w)
        lstr = l if isinstance(l, str) else float_to_si_string(l)
        nfstr = nf if isinstance(nf, str) else "%s" % int(nf)
        spstr = sp if isinstance(sp, str) else float_to_si_string(sp)
        stmstr = stm if isinstance(stm, str) else "%s" % int(stm)
        spmstr = spm if isinstance(spm, str) else "%s" % int(spm)
        sub_type = sub_type

        return dict(w=wstr, l=lstr, nf=nfstr, sp=spstr, stm=stmstr, spm=spmstr, sub_type=sub_type)

    def get_cell_name_from_parameters(self):
        # type: () -> str
        sub_type=self.params['sub_type']
        if sub_type=='ntap':
            return 'cap_mom_accurate_nw'
        else:
            return 'cap_mom_accurate'

    def is_primitive(self):
        # type: () -> bool
        return True

    def should_delete_instance(self):
        # type: () -> bool
        return self.params['w'] == 0 or self.params['l'] == 0 or self.params['nf'] == 0

class StdCellBase(Module):
    """The base design class for standard cells.

    Parameters
    ----------
    database : ModuleDB
        the design database object.
    yaml_file : str
        the netlist information file name.
    **kwargs :
        additional arguments
    """

    def __init__(self, database, yaml_file, **kwargs):
        Module.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            )

    @property
    def static(self):
        self._static=True

    def design(self):
        pass

    def get_schematic_parameters(self):
        # type: () -> Dict[str, str]
        return dict()
    
    def get_cell_name_from_parameters(self):
        raise ValueError('Implement in child class!')


    def is_primitive(self):
        # type: () -> bool
        return True

    def should_delete_instance(self):
        # type: () -> bool
        pass

class StdCellFFModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
            cleardown=False,
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            strength='Drive strength',
            cleardown='Generate with cleardown functionality?',
            negated_output='Generate with negated output?'
            )

    def get_cell_name_from_parameters(self):
        # type: () -> str
        strength = self.params['strength']
        cleardown = self.params['cleardown']
        negated_output = self.params['negated_output']
        if not isinstance(strength, int):
            if isinstance(strength, str):
                strength=int(strength)
            else:
                raise ValueError('Parameter strength should be integer or string')
        allowed = [0,1,2,4]
        if strength not in allowed:
            raise ValueError("Drive strength must be in %s" % allowed)
        if cleardown:
            return 'ff2_cdn_%sx' % strength
        else:
            if negated_output:
                return 'ff2_%sx' % strength
            else:
                return 'ff1_%sx' % strength

    def design(self,strength='1', cleardown=False, negated_output=False):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return self.params['strength'] == 0 or self.params['strength'] == '0'

class StdCellAndModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            strength='Drive strength',
            )

    def get_cell_name_from_parameters(self):
        # type: () -> str
        strength = self.params['strength']
        if not isinstance(strength, int):
            if isinstance(strength, str):
                strength=int(strength)
            else:
                raise ValueError('Parameter strength should be integer or string')
        allowed = [1,2,4,8,16]
        if strength not in allowed:
            raise ValueError("Drive strength must be in %s" % allowed)
        else:
            return 'and2_%sx' % strength

    def design(self,strength='1'):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return self.params['strength'] == 0 or self.params['strength'] == '0'

class StdCellBufModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            strength='Drive strength',
            )

    def get_cell_name_from_parameters(self):
        # type: () -> str
        strength = self.params['strength']
        if not isinstance(strength, int):
            if isinstance(strength, str):
                strength=int(strength)
            else:
                raise ValueError('Parameter strength should be integer or string')
        allowed = [0,1,2,3,4,6,8,12,16,20,24]

        if strength not in allowed:
            raise ValueError("Drive strength must be in %s" % allowed)
        else:
            return 'buf_%sx' % strength

    def design(self,strength='0'):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return self.params['strength'] == -1 or self.params['strength'] == '-1'

class StdCellDecapModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    @classmethod
    def get_default_param_values(cls):
        return dict(
        )

    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        return dict(
            size='Decap size',
            )

    def get_cell_name_from_parameters(self):
        # type: () -> str
        size = self.params['size']
        if not isinstance(size, int):
            if isinstance(size, str):
                size=int(size)
            else:
                raise ValueError('Parameter size should be integer or string')
        allowed = [1,4,8,16,32,64]

        if size not in allowed:
            raise ValueError("Decap size must be in %s" % allowed)
        else:
            return 'decap_%sx' % size

    def design(self,size='0'):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return self.params['size'] == 0 or self.params['size'] == '0'




class WelltapModule(StdCellBase):
    def __init__(self, database, yaml_file, **kwargs):
        StdCellBase.__init__(self, database, yaml_file, **kwargs)

    def get_cell_name_from_parameters(self):
        return 'welltap'

    def design(self):
        pass

    def should_delete_instance(self):
        # type: () -> bool
        return False
